var insignificant = 
{
	settings: {
		bgPath: undefined,
		setBackground: function( clickedObject ) {
			// use jQuery, because you can use #insignificant_main > #editor
			var topPos = insignificant.definition[ clickedObject.id ].y;
			document.getElementById('editor').style.backgroundPosition = "0 " + topPos;
			document.getElementById('selectedBackground').value = clickedObject.id;
			// toggle selectionborder
			$(".selected_thumb").toggleClass( "selected_thumb" );
			$("#" + clickedObject.id ).toggleClass( "selected_thumb" );
		},
		font: {
			setSize: function( element )
			{
				$("#textEditor").css( "font-size", element.options[element.selectedIndex].value + "px" );
			},
			setShadow: function( element ) {
				if( element.checked )
				{
					$("#textEditor").css( "text-shadow", "0px 0px 10px #222" );
				}
				else
				{
					$("#textEditor").css( "text-shadow", "none" );
				}
			}
		},
		setBalloon: function( element ) {
			if( element.checked )
			{
				$("#textEditor").css( "background", "#fff" );
			}
			else
			{
				$("#textEditor").css( "background", "none" );
			}
		},
		drawToolkit: function() {
			$("#insignificant_main #tools").append("<form>" +
				"<fieldset>" +
					"<div onclick=\"$('#balloon').click();\" title=\"Want a ballooooooon?\" class=\"icon\" id=\"balloonIcon\"></div><input type=\"checkbox\" onchange=\"insignificant.settings.setBalloon(this);\" id=\"balloon\" />" +
					"<div onclick=\"$('#textShadow').click();\" title=\"Text Shadow\" class=\"icon\" id=\"shadowIcon\">S</div><input type=\"checkbox\" onchange=\"insignificant.settings.font.setShadow(this);\" id=\"textShadow\" />" +
					"<ul id=\"font_ul\"><li class=\"title\">Font</li></ul>" +
					"<div class=\"input\" title=\"font size\"><ul class=\"fontSizeIcon\"><li>A</li><li>A</li><li>A</li></ul><select onchange=\"insignificant.settings.font.setSize(this);\" id=\"fontSize\">" +
			"			<option value=\"20\">20</option>" +
			"			<option selected=\"selected\" value=\"30\">30</option>" +
			"			<option value=\"40\">40</option>" +
			"			<option value=\"50\">50</option>" +
			"			<option value=\"60\">60</option>" +
			"			<option value=\"70\">70</option>" +
					"</select><span class=\"selectArrow\"></span></div>" +
				"<input type=\"text\" id=\"selectedX\" name=\"selectedX\" />"+
				"<input type=\"text\" id=\"selectedY\" name=\"selectedY\" />" +
				"<input type=\"text\" id=\"selectedBackground\" name=\"selectedBackground\" value=\"vader\" />" +
				"<div class=\"input\"><input type=\"button\" onclick=\"insignificant.generator.generate( this.form );\" value=\"Make insignificant!\" /></div>" +
				"</fieldset>" +
			"</form>");

			insignificant.settings.drawStyledText( "fontArial", $("#font_ul"), 
				"Arial", 50, "Arial", false );
			insignificant.settings.drawStyledText( "fontComicSans", $("#font_ul"), 
				"'Comic Sans MS'", 115, "Comic Sans", false );
			insignificant.settings.drawStyledText( "fontLucidaSans", $("#font_ul"), 
				"'Lucida Sans Unicode'", 130, "Lucida Sans", false );
			insignificant.settings.drawStyledText( "fontImpacted", $("#font_ul"), 
				"Impacted", 100, "Impacted", false );
			insignificant.settings.drawStyledText( "fontImpact", $("#font_ul"), 
				"Impact", 70, "Impact", true );
			insignificant.settings.drawStyledText( "fontVerdana", $("#font_ul"), 
				"Verdana", 100, "Verdana", false );

			$(".font_type").click(
				function(){
					$(".font_type").removeClass("selected");
					$(this).toggleClass("selected");
					$("#textEditor").css( "font-family", $(this).attr( "font" ) );
				}
			);
		},
		drawStyledText: function( id, jParent, style, width, text, selected ) {
			var className = "font_type";
			if( selected )
			{
				className += " selected";
			}
			var fontCanvas = $( document.createElement("canvas") );
			fontCanvas.attr("id", id)
			.attr("width", width)
			.attr("height", "25")
			.attr("font", style) // to retrieve selected font
			.attr("class", className);

			var fontLi = $( document.createElement("li") );
			jParent.append( fontLi.append( fontCanvas ) );

			var fontCanvas = fontCanvas[0].getContext("2d");
			fontCanvas.font = "20px " + style;
			fontCanvas.fillStyle = "#fff";
			/* Upon drawing webfonts, it's possible the font has not yet loaded on the page when
			this canvas (for the font preview) is drawn. A delay of 500 milliseconds is enough to
			load the font, at least on my local server. */
			setTimeout( function(){ fontCanvas.fillText(text, 5, 20); }, 500 );
		}
	},
	definition: {
		"vader":      { y: "0px" },
		"enterprise": {	y: "-380px" },
		"sargeras":   { y: "-760px" },
		"fluffy":     {	y: "-1140px" }
	},
	editor: {
		drawBall: function() {
			var context = $("#ball")[0].getContext("2d");
			// draw circle fill
			context.fillStyle = "#fff";
			context.beginPath();
			context.arc(10, 10, 9, 0, Math.PI*2, true);
			context.closePath();
			context.fill();
			// draw circle outline
			context.strokeStyle = "#000";
			context.lineWidth = 2;
			context.beginPath();
			context.arc(10, 10, 9, 0, Math.PI*2, true);
			context.closePath();
			context.stroke();
		},
		getMousePos: function(event) {
			// Thanks to Ryan Artecona on http://stackoverflow.com/a/5932203
			var totalOffsetX = 0;
			var totalOffsetY = 0;
			var currentElement = $("#editor")[0];

			do{
				totalOffsetX += currentElement.offsetLeft;
				totalOffsetY += currentElement.offsetTop;
			}
			while(currentElement = currentElement.offsetParent)

			var mousePosX = event.pageX - totalOffsetX;
			var mousePosY = event.pageY - totalOffsetY;
			return {x: mousePosX, y: mousePosY};
		},
		dropAction: function (event) {
			var mousePos = insignificant.editor.getMousePos(event);
			insignificant.editor.moveDragItem(mousePos.x, mousePos.y);
			return false;
		},
		moveDragItem: function(x, y) {
			var dragItem = $("#dragItem")[0];
			// save selected positions
			$("#selectedY").val( y );
			$("#selectedX").val( x );
			// update position in editor
			dragItem.style.marginTop = y + "px";
			dragItem.style.marginLeft = x + "px";
		}
	},
	init: function( bgPath ) {
		// store the background path and initialize the background in the editor
		insignificant.settings.bgPath = bgPath;
		$("#insignificant_main > #editor").css("background-image","url('"+bgPath+"')");
		$("#insignificant_main .bgth").css("background-image","url('"+bgPath+"')");

		// init position in editor
		insignificant.editor.moveDragItem(140,40);

		// Make the textbox draggable
		$("#dragItem").bind("dragstart", function(event) {
			event.originalEvent.dataTransfer.setData("Text","");
		});

		// Allow the textbox to be dropped on the editor
		var jEditor = $("#editor");
		jEditor.bind("dragover dragenter", function(event) {
			event.originalEvent.preventDefault();
		});
		jEditor.bind("drop", function(event) {
			insignificant.editor.dropAction(event.originalEvent);
		});

		// Draw ball shaped draghandle for the textarea
		insignificant.editor.drawBall();

		insignificant.settings.drawToolkit();
	},
	generator: {
		drawBalloon: function(ctx, dragLeft, dragTop) {
			var width = 400;
			var height = 200;
			var radius = 10;
			ctx.beginPath();
			ctx.moveTo(           dragLeft + radius, dragTop );

			ctx.lineTo(           dragLeft + width - radius, dragTop );
			ctx.quadraticCurveTo( dragLeft + width,          dragTop, dragLeft + width, dragTop + radius );

			ctx.lineTo(           dragLeft + width, dragTop + height - radius );
			ctx.quadraticCurveTo( dragLeft + width, dragTop + height, 
				dragLeft + width - radius, dragTop + height );

			ctx.lineTo(           dragLeft + radius, dragTop + height );
			ctx.quadraticCurveTo( dragLeft,          dragTop + height, dragLeft, dragTop + height - radius );

			ctx.lineTo(           dragLeft, dragTop + radius );
			ctx.quadraticCurveTo( dragLeft, dragTop, dragLeft + radius, dragTop );

			ctx.closePath();

			// Fill the path
			ctx.fillStyle = "#fff";
			ctx.fill();
		},
		generate: function() {
			// Create a rebuild-canvas. In this canvas the user configuration will be rebuild.
			var rebuildCanvas = $( document.createElement("canvas") );
			rebuildCanvas.attr("id", "rebuildCanvas")
			.attr("width","600")
			.attr("height","380");

			var rebuildContext = rebuildCanvas[0].getContext("2d");
			var background = new Image();
			background.src = insignificant.settings.bgPath;
			var yOffset = parseInt( insignificant.definition[$("#selectedBackground").val()].y );
			rebuildContext.drawImage( background, 0, yOffset );

			var dragItem = $("#dragItem")[0];
			var dragLeft = parseInt(dragItem.style.marginLeft)
			var dragTop = parseInt(dragItem.style.marginTop);

			if( $("#balloon")[0].checked )
			{
				insignificant.generator.drawBalloon(rebuildContext, dragLeft, dragTop);
			}

			if( $("#textShadow")[0].checked )
			{
				rebuildContext.shadowOffsetX = '0';
				rebuildContext.shadowOffsetY = '0';
				rebuildContext.shadowBlur = '10';
				rebuildContext.shadowColor = '#222';
			}

			var fontType = $(".font_type.selected").attr( "font" );

			rebuildContext.font = $("#fontSize").val() + "px " + fontType;
			rebuildContext.fillStyle = "#fff";

			var lineHeight = $("#fontSize").val();
			var balloonWidth = 400;
			var balloonTopMargin = dragTop + parseInt(lineHeight) + 20;
			insignificant.multiFillText(rebuildContext, $("#textEditor").val(),
				dragLeft, balloonTopMargin, lineHeight, balloonWidth);

			// Copy the rebuild-canvas to a PNG as a base64 encoded string.
			var dataUrl = rebuildCanvas[0].toDataURL("image/jpg");
			var generateImgUrl = dataUrl;

			// Open lightbox
			$("body").append("<div id=\"insignificant_generated\">" +
				"<div>" +
					"<img src=\""+generateImgUrl+"\" id=\"generatedImage\" alt=\"Generated Image\" />" +
					"<span onclick=\"insignificant.generator.close( this.form );\">close</span>" +
				"</div>" +
			"</div>");			
		},
		close : function() {
			$("#insignificant_generated").remove();
		}
	},
	// MultiFillText - Automatically split text over multiple lines, source: http://jsfiddle.net/jeffchan/WHgaY/76/
	multiFillText: function(ctx, text, x, y, lineHeight, fitWidth) {
	    var draw = x !== null && y !== null;

	    text = text.replace(/(\r\n|\n\r|\r|\n)/g, "\n");
	    sections = text.split("\n");

	    var i, str, wordWidth, words, currentLine = 0,
		maxHeight = 0,
		maxWidth = 0;

	    var printNextLine = function(str) {
		if (draw) {
		    ctx.fillText(str, x, y + (lineHeight * currentLine));
		}

		currentLine++;
		wordWidth = ctx.measureText(str).width;
		if (wordWidth > maxWidth) {
		    maxWidth = wordWidth;
		}
	    };

	    for (i = 0; i < sections.length; i++) {
		words = sections[i].split(' ');
		index = 1;

		while (words.length > 0 && index <= words.length) {

		    str = words.slice(0, index).join(' ');
		    wordWidth = ctx.measureText(str).width;

		    if (wordWidth > fitWidth) {
		        if (index === 1) {
		            // Falls to this case if the first word in words[] is bigger than fitWidth
		            // so we print this word on its own line; index = 2 because slice is
		            str = words.slice(0, 1).join(' ');
		            words = words.splice(1);
		        } else {
		            str = words.slice(0, index - 1).join(' ');
		            words = words.splice(index - 1);
		        }

		        printNextLine(str);

		        index = 1;
		    } else {
		        index++;
		    }
		}

		// The left over words on the last line
		if (index > 0) {
		    printNextLine(words.join(' '));
		}
	    }

	    maxHeight = lineHeight * (currentLine);

	    if (!draw) {
		return {
		    height: maxHeight,
		    width: maxWidth
		};
	    }
	}
};
