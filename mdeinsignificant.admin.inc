<?php

/**
 * Insignificant Admin Menu 
 * To clear the menu cache:
 *  Administer >> Site Configuration >> Performance
 *  click the "Clear cached data" button.
 * see http://drupal.org/node/206761 
*/
function mdeinsignificant_admin() 
{
	$form = array();


	$form['mastersprite'] = array(
		'#type' => 'fieldset',
		'#title' => t('Master Sprite settings'),
		'#collapsible' => TRUE,
	);
	$form['mastersprite']['mdeinsignificant_mastersprite'] = array(
		'#type' => 'textfield',
		'#title' => t('Master Sprite'),
		'#default_value' => variable_get('mdeinsignificant_mastersprite', 'http://localhost/drupal/sites/all/modules/mdeinsignificant/includes/backgrounds.jpg'),
		'#size' => 100,
		'#maxlength' => 200,
		'#description' => t("The location of the master sprite on the server."),
		'#required' => TRUE,
	);

	/*
	$form['bg1'] = array(
		'#type' => 'fieldset',
		'#title' => t('Background 1 settings'),
		'#collapsible' => TRUE,
	);
	$form['bg1']['mdeinsignificant_bg1_y'] = array(
		'#type' => 'textfield',
		'#title' => t('Top y coordinate'),
		'#default_value' => variable_get('mdeinsignificant_bg1_y', '0'),
		'#size' => 100,
		'#maxlength' => 200,
		'#required' => TRUE,
	);
	$form['bg1']['mdeinsignificant_bg1_thx'] = array(
		'#type' => 'textfield',
		'#title' => t('Thumbnail left x coordinate'),
		'#default_value' => variable_get('mdeinsignificant_bg1_thx', '0'),
		'#size' => 100,
		'#maxlength' => 200,
		'#required' => TRUE,
	);
	$form['bg1']['mdeinsignificant_bg1_thy'] = array(
		'#type' => 'textfield',
		'#title' => t('Thumbnail top y coordinate'),
		'#default_value' => variable_get('mdeinsignificant_bg1_thy', '0'),
		'#size' => 100,
		'#maxlength' => 200,
		'#required' => TRUE,
	);

	$form['bg2'] = array(
		'#type' => 'fieldset',
		'#title' => t('Background 2 settings'),
		'#collapsible' => TRUE,
	);
	$form['bg2']['mdeinsignificant_bg2_y'] = array(
		'#type' => 'textfield',
		'#title' => t('Top y coordinate'),
		'#default_value' => variable_get('mdeinsignificant_bg2_y', '0'),
		'#size' => 100,
		'#maxlength' => 200,
		'#required' => TRUE,
	);
	$form['bg2']['mdeinsignificant_bg2_thx'] = array(
		'#type' => 'textfield',
		'#title' => t('Thumbnail left x coordinate'),
		'#default_value' => variable_get('mdeinsignificant_bg2_thx', '0'),
		'#size' => 100,
		'#maxlength' => 200,
		'#required' => TRUE,
	);
	$form['bg2']['mdeinsignificant_bg2_thy'] = array(
		'#type' => 'textfield',
		'#title' => t('Thumbnail top y coordinate'),
		'#default_value' => variable_get('mdeinsignificant_bg2_thy', '0'),
		'#size' => 100,
		'#maxlength' => 200,
		'#required' => TRUE,
	);
	*/

	return system_settings_form($form);
}
